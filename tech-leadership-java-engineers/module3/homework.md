# Epics

## 1. User interface and Navigation
The new e-commerce should provide a easy way to navigate and use the we site. The web site should follow web accessibility.

## 2. Project infrastructure
Create the new repository or the project and setup the basic CI/CD

## 3. Home page
Creation of the home page with all the links that will be used in the future. The home page must follow the user interface mockup implemented on Epic 1

## 4. Product Listing
In the main page a listing with 50 products must be shown. The backend must provide the API for the frontend to be able to fetch the products. The user will be able to filter search products by bran and category, search options will be available as well.
When user click on a product the website will redirect him to the product details page. In the product details page we will show similar products for the user, based on the category.

## 5. Shopping Cart
The user will be able to add products to the shopping cart from the detail page or the main page. A new shopping cart page will show all the products that the user added and the total amount to pay.

## 6. Checkout page
In the checkout page the user must be able to select payment type and proceed with the checkout. After success payment the user should receive an email with order details. In case of issues with the payment, the user will receive a message in the website and also an email with details about what went wrong.

## Epics estimation
| Epic  | Size  |
|---|---|
| User interface and Navigation   | S  |
| Project infrastructure  |  XS |
| Home page  |  XS |   |
| Product Listing  | M  |
| Shopping Cart  | M  |
| Checkout page  |  L |

# User stories

## EPIC 1 - User interface and Navigation

### User Story 1
E-commerce website navigation flow

### User Story 2
Main page design/mockup

### User Story 3
Search page design/mockup

### User Story 4
Product detail page design/mockup

### User Story 5
Shopping cart and checkout page design.mockup

## EPIC 2 - Project infrastructure

### User Story 1
As a developer, I need a repository, so that I can push the code changes

### User Story 2
As a developer,I need a CI/CD configured in the repository, so that I can check the code builds

## EPIC 3 - Home page

### User Story 1
As a developer, I need the frontend setup ready, so that I can start to add pages to the application.

### User Story 2
As a user, I want to be able to access the e-commerce home page, so that I can navigate to other pages.

## EPIC 4. Product Listing

### User Story 1
As a Frontend developer, I want to be able to fetch products with pagination and filter capabilities, so that I can create the list of products to show to the website user

### User Story 2
As an user, I want to be able to filter products by brand and category, so that I can find the product I want.

### User Story 3
As a Frontend developer, I want to be able to fetch data of related products of a given product, so that I can list the related products.

### User Story 4
As an user, I want to be able to click on the product and be redirect to details page, so that I can see the product details and related products.

## EPIC 5 - Shopping Cart

### User Story 1
As an user, I want to be able to add products to my shopping cart, so that I can proceed to checkout further

### User Story 2
As an user, I want to be able to manage the items on my shopping cart, so that I can remove/add and see the total amount to be paid


## EPIC 6 - Checkout page

### User Story 1
As an application, I want to be able to integrate with a payment gateway, so that I can finalize orders coming from the website

### User Story 2
As an application, I want to be able to send emails, so that I can notify users about successful or failed orders

### User Story 3
As an user, I want to be able to select the payment type and checkout, so that I can by a product

### Estimation of users stories considering a new team
This estimation technique proposed for a new team is **Bucket System**

| Epic  | US | Size  |
|---|---|---|
| Epic 1   | US 1  | 1 |
| Epic 1   | US 2  | 3 |
| Epic 1   | US 3  | 5 |
| Epic 1   | US 4  | 3 |
| Epic 2   | US 1  | 1 |
| Epic 2   | US 2  | 8 |
| Epic 3   | US 1  | 3 |
| Epic 3   | US 3  | 5 |
| Epic 4   | US 1  | 13 |
| Epic 4   | US 2  | 5 |
| Epic 4   | US 3  | 13 |
| Epic 4   | US 4  | 2 |
| Epic 5   | US 1  | 13 |
| Epic 5   | US 2  | 5 |
| Epic 6   | US 1  | 20 |
| Epic 6   | US 2  | 13 |
| Epic 6   | US 3  | 8 |
|          | Total | 75 |

### Estimation of users stories considering a established team
This estimation technique proposed for a established team is **Planning Poker**

| Epic  | US | Size  |
|---|---|---|
| Epic 1   | US 1  | 1/2 |
| Epic 1   | US 2  | 2 |
| Epic 1   | US 3  | 3 |
| Epic 1   | US 4  | 2 |
| Epic 2   | US 1  | 0 |
| Epic 2   | US 2  | 1 |
| Epic 3   | US 1  | 2 |
| Epic 3   | US 3  | 3 |
| Epic 4   | US 1  | 8 |
| Epic 4   | US 2  | 3 |
| Epic 4   | US 3  | 8 |
| Epic 4   | US 4  | 1 |
| Epic 5   | US 1  | 5 |
| Epic 5   | US 2  | 2 |
| Epic 6   | US 1  | 13 |
| Epic 6   | US 2  | 8 |
| Epic 6   | US 3  | 1 |
|          | Total | 62,5 |

### Number of Sprint to finish the project
- The new team would need around 5 (+1) Sprints to finish the work. I'm considering a pace of 15 story points per Sprint. +1 Spring is added considering a risk of the new team get blocked because of some impediment because of lack of knowledge and wrong estimations.
- The established team would need around 4 Sprints to finish the work. I'm considering a pace of 15 story points per Sprint. We are not considering some extra Sprint this time because the team has a great experience and usually when the estimation is wrong, it is because they finish the work in less time than the expectation

## Apartment Renovation

### WBS
![WBS](./wbs.jpeg "WBS")


### Dependencies and Critical path

![Dependencies and Critical path](./dependecy_criticalpath.jpeg "Dependencies and Critical path")

### Gantt chart

![Gantt chart](./gantt.png "Gantt chart")
