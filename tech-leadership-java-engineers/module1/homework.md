# Solution Architecture Documenting Practical Task

## Task 1 - Initial design

### Sub-task 1.1

#### Gaps/Unclear requirements
- The entity can evolve frequently? 
- New entities can appear in the near feature?
- Authentication is needed? If so, what type of authentication we suppose to use? Basic Authentication, OAuth2 or other?
- Which attributes we need to consider for **Search** feature? Full text search is necessary?
- In there any preferred language?
- The application should run on premisses or in a cloud provider?

### Sub-task 1.2 

#### Components
```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

AddElementTag("v1.0", $borderColor="#d73027")
AddElementTag("v1.1", $fontColor="#d73027")

AddRelTag("backup", $textColor="orange", $lineColor="orange", $lineStyle = DashedLine())

System_Boundary(c1, "Event System") {
  Container(api, "REST API", "Java - Spring Boot Application", "Handles business logic and authentication")
}


Container_Ext(user, "External Application", "External application consuming the API")
Container_Ext(db, "Database", "MongoDB Cloud", "Holds events data, creation, update, delete and read")

Rel(user, api, "Uses", "https")
Rel_R(api, db, "Reads/Writes")

SHOW_LEGEND()
@enduml
```

- **REST API**
 
  For the REST API component the technology chosen was Java 17 with the Spring Boot Framework. This decision was mande taking in consideration that the team has lots of experience with this technology and we don't need to spend much time learning new technologies.
- **Database**

  For the database MongoDB was choose because it attends the requirements of data volume and has the capabilities for multi-tenant, minimize schema mapping, search, filter, full text search if needed and also provides backup policy.

### Sub-task 1.3

#### Create new event use case
```plantuml
@startuml
User-> "REST API" : Create event request
"REST API"-> "MongoDB Database" : Save new event
"REST API"-> User: New event create
@enduml
```
#### Search event use cases
```plantuml
@startuml
User-> "REST API" : search event request
"REST API"-> "MongoDB Database" : query database
"REST API"-> User: returns list of events
@enduml
```

## Task 2 - DB  choice

### Title
Database selection to store Events resource data.
### Status:
Done

### Background: 
The application needs a database to store Event resource data and provide CRUD operations with fast read executions for search operations

### Functional requirements:
- Data storage should support automated data backup every 30 days.
- Data storage for REST service should provide support multi-tenant content storage.

### Non-functional requirements
- Event Model for Data Storage Schema mapping complexity should be minimized.
- Data storage should support detailed logging for troubleshooting.
- The infrastructure should support two European regions: east and west.

### Quality attributes
- Up to 100 000 000 of records are stored
- CRUD operations and typical search queries should execute within 1 second

### Solution Options
- MongoDB
- PostgreSQL
- Elastic Search
### Decision criteria
We considered using a database that attends to our requirements, which the development team is familiar with, and provides a simple implementation.

The team has experience with PostgreSQL but we don't have many entities to manage, we don't have relationships between tables and we need a simple schema mapping, in this case, it is not a good match for our requirements to keep simplicity.

Of the options that are left, we choose MongoDB. Although Elastic Search provides advanced searching and analytical capabilities, we don't need those features to meet our requirements. In this case, MongoDB fulfills our needs and the development team already has experience with it.

### Decision
Decided on MongoDB. Open to new choices in the future if it is needed.

## (Optional) Task 2 - context view
#### Context Diagram
```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

AddElementTag("v1.0", $borderColor="#d73027")
AddElementTag("v1.1", $fontColor="#d73027")

AddRelTag("backup", $textColor="orange", $lineColor="orange", $lineStyle = DashedLine())

System(api, "Event API", "API to handle events")


Person(user, "User", "User access event API through external system")
System_Ext(db, "Database", "MongoDB Cloud")

Rel(user, api, "Uses", "https")
Rel_R(api, db, "Reads/Writes")

SHOW_LEGEND()
@enduml
```


## References:
- https://spring.io/projects/spring-boot
- https://spring.io/projects/spring-data-mongodb
- https://www.mongodb.com/pricing
- https://www.mongodb.com/docs/atlas/build-multi-tenant-arch/#:~:text=The%20multi%2Dtenancy%20logic%20exists,issues%20in%20the%20long%2Dterm.
- https://www.elastic.co/getting-started?utm_campaign=B-Stack-Trials-EMEA-C-Exact&utm_content=Stack-WhatisStack&utm_source=google&utm_medium=cpc&device=c&utm_term=what%20is%20elasticsearch&gad_source=1&gclid=CjwKCAjwvrOpBhBdEiwAR58-3K7-aLYlsH7DsgzXBQrKF_yQ3Mh2uKQjtx-f73M-uFWVVE6NOR-ocRoCWTcQAvD_BwE