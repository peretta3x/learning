# Practice
Look carefully on discovery artifacts and do the following:

- Try to identify architecture styles being used in legacy and proposed solutions. List benefits and limitations for each of the identified style.
- Try to figure out what architecture patterns has been applied within solution. List what are the problem given pattern is intended to solve.

## Existent Solution
### Architecture Style
- Monolith: 
    - Benefits
        - Easy to implement
        - Easy to understand from the architecture point of view
    - Limitations
        - If it gets too big, becomes hard to maintain and introduce new features
        - Continuous deployment is difficult
        - If scale is needed, the whole application is scale up when just some specific module is the bottleneck
- Layered architecture
  - Benefits
    - Separation of concerns
    - Well-defined interfaces between layers
    - Reduces complexity, improves modularity, reusability, maintainability; though, it can affect performance
  - Limitations
    - If skipping layers is used too much it can become a big mall of mud
- REST
  - Benefits
    - The strong decoupling of client and server together with the text-based transfer of information
    - Easier to read the information compared to SOAP web services
  - Limitations
    - If you have requests that execute resource intensive operations, it may affect the performance of the system
    - Most of the implementations does not provided a way to filter the properties by client to avoid fetching unnecessary data
uniform addressing protocol provided the basis for meeting the requirements of the Web
### Architecture Patterns
- Static Content Hosting (NGINX)
    - Delivers static content. This can reduce the need for potentially expensive compute

## Proposed Solution
### Architecture Style
- Microservices
    - Benefits 
      - High modularity and easy to change
      - Ease to split scope between teams
      - Improve system's fault tolerance
      - Easier to test
    -  Decomposition of services is as challenge
    -  Hard to manage multiple services, sometime a dedicated Devops team might be necessary
    -  Deployment is complex compared to monolith
- Event Driven
  - Benefits
    - Loosely-coupled
    - Easier to get scalability and performance
    - In general, it has good deployment perspective due all services is decoupled
  - Limitations
    - Complex to implement because of distributed nature
    - Distributed transactions may be a challenge
    - Manage events may be a challenge
    - Hard to test and debug

### Architecture Patterns
- Federated Identity
  - Delegate authentication to an external identity provider. This can simplify development, minimize the requirement for user administration, and improve the user experience of the application.
- Static Content Hosting (NGINX)
    - Delivers static content. This can reduce the need for potentially expensive compute
- Stranger Fig
  - Incrementally migrate a legacy system by gradually replacing specific pieces of functionality with new applications and services
- Backend for Frontend
  - This pattern is useful when you want to avoid customizing a single backend for multiple interfaces.
- Cache-Aside Pattern
  - Load data on demand into a cache from a data store. This can improve performance and also helps to maintain consistency between data held in the cache and data in the underlying data store.
- Publish/Subscribe
  - Enable an application to announce events to multiple interested consumers asynchronously, without coupling the senders to the receivers.
- CQRS
  - Separates reads and writes into different models, using commands to update data, and queries to read data.
- Gateway Routing
    - Route requests to multiple services or multiple service instances using a single endpoint. 
- Geode
  - The Geode pattern involves deploying a collection of backend services into a set of geographical nodes