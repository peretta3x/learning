# Couchbase Task

### How to execute the task ###
In order to run the application first you need to start the Couchbase using docker-compose.
```bash
docker-compose up
```

### User API ###
To start the user API you can run the maven command:
```bash
./mvnw spring-boot:run
```

### API Testing ###
In order to test the API you can run the commands defined in the [api.http](./api.http) file.
