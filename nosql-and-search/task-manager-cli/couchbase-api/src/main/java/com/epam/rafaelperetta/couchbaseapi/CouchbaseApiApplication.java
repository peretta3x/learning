package com.epam.rafaelperetta.couchbaseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CouchbaseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CouchbaseApiApplication.class, args);
	}

}
