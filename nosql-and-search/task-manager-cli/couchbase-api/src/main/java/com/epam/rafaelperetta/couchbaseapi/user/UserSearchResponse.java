package com.epam.rafaelperetta.couchbaseapi.user;

public record UserSearchResponse(String id, String fullName, String email) {

    public UserSearchResponse withId(String id) {
        return new UserSearchResponse(id, this.fullName, this.email);
    }

}
