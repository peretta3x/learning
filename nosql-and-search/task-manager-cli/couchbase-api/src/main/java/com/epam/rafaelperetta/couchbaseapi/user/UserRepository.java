package com.epam.rafaelperetta.couchbaseapi.user;

import java.util.List;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.data.couchbase.repository.Query;

public interface UserRepository extends CouchbaseRepository<User, String> {

    // Add custom query methods if needed
    @Query("#{#n1ql.selectEntity} WHERE email = $1 LIMIT 1")
    User findUserByEmail(String email);

    @Query("#{#n1ql.selectEntity} WHERE sport.name = $1")
    List<User> findBySportName(String sportName);
}