package com.epam.rafaelperetta.couchbaseapi.user;

import java.util.List;

import org.springframework.stereotype.Service;

import com.couchbase.client.core.error.CouchbaseException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.search.SearchOptions;
import com.couchbase.client.java.search.SearchQuery;
import com.couchbase.client.java.search.result.SearchResult;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final Cluster cluster;

    public UserService(UserRepository userRepository, Cluster cluster) {
        this.userRepository = userRepository;
        this.cluster = cluster;
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public User findUserById(String id) {
        return userRepository.findById(id).orElse(null);
    }

    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public List<User> getUsersBySportName(String sportName) {
        return userRepository.findBySportName(sportName);
    }

    public List<UserSearchResponse> search(String query) {
        try {
            final SearchResult result = cluster.searchQuery("fts_user", SearchQuery.queryString(query),
                    SearchOptions.searchOptions().fields("fullName", "email"));

            return result.rows().stream()
                    .map(row -> row.fieldsAs(UserSearchResponse.class).withId(row.id()))
                    .toList();

        } catch (CouchbaseException ex) {
            ex.printStackTrace();
            return List.of();
        }
    }
}