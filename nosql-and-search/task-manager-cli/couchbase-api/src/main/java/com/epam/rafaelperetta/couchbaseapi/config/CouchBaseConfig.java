package com.epam.rafaelperetta.couchbaseapi.config;

import java.time.Duration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;

import com.couchbase.client.java.env.ClusterEnvironment;

@Configuration
public class CouchBaseConfig extends AbstractCouchbaseConfiguration {

    @Override
    public String getConnectionString() {
        return "couchbase://localhost";
    }

    @Override
    public String getUserName() {
        return "admin";
    }

    @Override
    public String getPassword() {
        return "admin123";
    }

    @Override
    public String getBucketName() {
        return "nosql";
    }

    @Override
    protected void configureEnvironment(final ClusterEnvironment.Builder builder) {
        builder.timeoutConfig(
                tconfig -> tconfig.connectTimeout(Duration.ofSeconds(15)).queryTimeout(Duration.ofSeconds(10)));
    }

}