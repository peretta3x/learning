package com.epam.rafaelperetta.taskmanager.commands;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.table.TableBuilder;

import com.epam.rafaelperetta.taskmanager.Task;

@ShellComponent
public class TaskCommand {

    private final CommandService commandService;

    private final MongoTemplate mongoTemplate;

    public TaskCommand(CommandService commandService, MongoTemplate mongoTemplate) {
        this.commandService = commandService;
        this.mongoTemplate = mongoTemplate;
    }

    @ShellMethod(key = "list", value = "List all tasks")
    public void list() {
        List<Task> tasks = this.mongoTemplate.findAll(Task.class);
        if (tasks.isEmpty()) {
            System.out.println("No tasks found");
            return;
        }
        TableBuilder tableBuilder = commandService.listToArrayTableModel(tasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "overdue", value = "List all overdue tasks")
    public void listOverdueTasks() {
        Criteria criteria = Criteria.where("deadline").lt(LocalDate.now());
        Query query = Query.query(criteria);

        List<Task> tasks = mongoTemplate.find(query, Task.class);
        if (tasks.isEmpty()) {
            System.out.println("No overdue tasks");
            return;
        }
        TableBuilder tableBuilder = commandService.listToArrayTableModel(tasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "list-by-category", value = "List all tasks of the given category")
    public void listByCategory(@ShellOption String category) {
        List<Task> tasks = this.mongoTemplate.find(
                Query.query(Criteria.where("category").regex("^" + category + "$", "i")),
                Task.class);

        if (tasks.isEmpty()) {
            System.out.println("No tasks registered with category: %s".formatted(category));
            return;
        }
        TableBuilder tableBuilder = commandService.listToArrayTableModel(tasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "list-substasks-by-category", value = "List all subtasks of the given category")
    public void listSubtasksByCategory(@ShellOption String category) {
        List<Task> tasks = this.mongoTemplate.find(
                Query.query(Criteria.where("category").regex("^" + category + "$", "i")),
                Task.class);

        if (tasks.isEmpty()) {
            System.out.println("No tasks registered with category: %s".formatted(category));
            return;
        }

        List<SubTask> subtasks = tasks.stream()
                .filter(task -> Objects.nonNull(task.getSubtasks()))
                .flatMap(task -> task.getSubtasks().stream())
                .toList();

        if (subtasks.isEmpty()) {
            System.out.println("No substasks found");
            return;
        }

        TableBuilder tableBuilder = commandService.listSubTaskToArrayTableModel(subtasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "search", value = "search task by description")
    public void searchTasksByDescription(@ShellOption String searchTerm) {
        TextCriteria criteria = TextCriteria.forLanguage("english").matching(searchTerm);
        Query query = TextQuery.queryText(criteria).sortByScore();
        List<Task> tasks = this.mongoTemplate.find(query, Task.class);

        if (tasks.isEmpty()) {
            System.out.println("No tasks registered with description containing: %s".formatted(searchTerm));
            return;
        }
        TableBuilder tableBuilder = commandService.listToArrayTableModel(tasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "search-subtask", value = "search task by name")
    public void searchSubtaskByName(@ShellOption String searchTerm) {
        Criteria criteria = new Criteria().orOperator(
                Criteria.where("subtasks.name").regex(searchTerm, "i"));

        Query query = new Query(criteria);

        List<Task> tasks = mongoTemplate.find(query, Task.class);
        List<SubTask> subtasks = tasks.stream().flatMap(task -> task.getSubtasks().stream()).toList();

        if (subtasks.isEmpty()) {
            System.out.println("No substasks found");
            return;
        }

        TableBuilder tableBuilder = commandService.listSubTaskToArrayTableModel(subtasks);
        System.out.println(tableBuilder.build().render(120));
    }

    @ShellMethod(key = "add", value = "Add new task")
    public void addTask(
            @ShellOption String name,
            @ShellOption String description,
            @ShellOption String category,
            @ShellOption String deadline) {

        Task task = new Task(null, name, description, LocalDate.now(), LocalDate.parse(deadline), category);
        this.mongoTemplate.insert(task);
        System.out.println("New task added!");

    }

    @ShellMethod(key = "add-subtask", value = "Add new subtask")
    public void addSubTask(
            @ShellOption String taskId,
            @ShellOption String name,
            @ShellOption String description) {

        Task task = this.mongoTemplate.findById(taskId, Task.class);
        if (Objects.isNull(task)) {
            System.out.println("Task with id %s does not exist".formatted(taskId));
            return;
        }
        task.addSubTask(new SubTask(null, name, description));
        this.mongoTemplate.save(task);
        System.out.println("New subtask added!");
    }

    @ShellMethod(key = "delete", value = "Delete task")
    public void deleteTask(@ShellOption String id) {
        Task task = this.mongoTemplate.findById(id, Task.class);
        if (Objects.isNull(task)) {
            System.out.println("Task with id %s does not exist".formatted(id));
            return;
        }
        this.mongoTemplate.remove(task);
        System.out.println("Task removed");
    }

    @ShellMethod(key = "delete-subtask", value = "Delete subtask")
    public void deleteSubtask(@ShellOption int index, @ShellOption String taskId) {
        Task task = this.mongoTemplate.findById(taskId, Task.class);
        if (Objects.isNull(task)) {
            System.out.println("Task with id %s does not exist".formatted(taskId));
            return;
        }

        var subtasks = task.getSubtasks();
        if (subtasks.size() < index) {
            System.out.println("Invalid index provided");
            return;
        }

        subtasks.remove(index - 1);
        this.mongoTemplate.save(task);
        System.out.println("Subtask removed");
    }

    @ShellMethod(key = "update", value = "Updates a task")
    public void updateTask(
            @ShellOption String id,
            @ShellOption String name,
            @ShellOption String description,
            @ShellOption String category,
            @ShellOption String deadline) {

        Task task = this.mongoTemplate.findById(id, Task.class);
        if (Objects.isNull(task)) {
            System.out.println("Task with id %s does not exist".formatted(id));
            return;
        }
        task.setName(name);
        task.setDescripton(description);
        task.setCategory(category);
        task.setDeadline(LocalDate.parse(deadline));
        this.mongoTemplate.save(task);
        System.out.println("New %s added has been updated!".formatted(id));
    }

    @ShellMethod(key = "update-subtask", value = "Update subtask")
    public void updateSubtask(
            @ShellOption int index,
            @ShellOption String taskId,
            @ShellOption String name,
            @ShellOption String description) {

        Task task = this.mongoTemplate.findById(taskId, Task.class);
        if (Objects.isNull(task)) {
            System.out.println("Task with id %s does not exist".formatted(taskId));
            return;
        }

        var subtasks = task.getSubtasks();
        if (subtasks.size() < index) {
            System.out.println("Invalid index provided");
            return;
        }

        SubTask updatedSubtask = subtasks.get(index - 1);
        updatedSubtask.setName(name);
        updatedSubtask.setDescription(description);

        this.mongoTemplate.save(task);
        System.out.println("Subtask updated");
    }

}
