package com.epam.rafaelperetta.taskmanager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.epam.rafaelperetta.taskmanager.commands.SubTask;

@Document("tasks")
public class Task {

    @Id
    private String id;

    private String name;

    @TextIndexed
    private String descripton;

    @Field(name = "created_at")
    private LocalDate createdAt;

    private LocalDate deadline;

    private String category;

    private List<SubTask> subtasks;

    public Task(String id, String name, String descripton, LocalDate createdAt, LocalDate deadline, String category) {
        this.id = id;
        this.name = name;
        this.descripton = descripton;
        this.createdAt = createdAt;
        this.deadline = deadline;
        this.category = category;
    }

    public Task(String id, String name, String descripton, LocalDate createdAt, LocalDate deadline, String category,
            List<SubTask> subTasks) {
        this.id = id;
        this.name = name;
        this.descripton = descripton;
        this.createdAt = createdAt;
        this.deadline = deadline;
        this.category = category;
        this.subtasks.forEach(subtask -> subtask.setTaskId(id));
    }

    public Task() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<SubTask> getSubtasks() {
        return subtasks;
    }

    public void addSubTask(SubTask subTask) {
        if (this.subtasks == null) {
            this.subtasks = new ArrayList<>();
        }
        subTask.setTaskId(this.id);
        this.subtasks.add(subTask);
    }

}
