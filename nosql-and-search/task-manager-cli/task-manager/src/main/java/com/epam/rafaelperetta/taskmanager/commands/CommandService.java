package com.epam.rafaelperetta.taskmanager.commands;

import java.util.ArrayList;
import java.util.List;

import org.springframework.shell.table.ArrayTableModel;
import org.springframework.shell.table.BorderStyle;
import org.springframework.shell.table.TableBuilder;
import org.springframework.stereotype.Service;

import com.epam.rafaelperetta.taskmanager.Task;

@Service
public class CommandService {

    public TableBuilder listToArrayTableModel(List<Task> tasks) {
        var header = new String[] { "ID", "NAME", "DESCRIPTION", "CREATED AT", "DEADLINE", "CATEGORY" };
        var tasksStringArray = tasks.stream()
                .map(v -> new String[] { v.getId(), v.getName(), v.getDescripton(), v.getCreatedAt().toString(),
                        v.getDeadline().toString(), v.getCategory() })
                .toList();

        var tableData = new ArrayList<String[]>();
        tableData.add(header);
        tableData.addAll(tasksStringArray);
        ArrayTableModel model = new ArrayTableModel(tableData.toArray(String[][]::new));
        TableBuilder tableBuilder = new TableBuilder(model);
        tableBuilder.addInnerBorder(BorderStyle.fancy_light_double_dash);
        return tableBuilder;

    }

    public TableBuilder listSubTaskToArrayTableModel(List<SubTask> subTasks) {
        var header = new String[] { "INDEX", "TASK ID", "NAME", "DESCRIPTION" };
        var subtasksStringArray = new ArrayList<String[]>();

        for (int i = 0; i < subTasks.size(); i++) {
            subtasksStringArray
                    .add(new String[] { String.valueOf(i + 1), subTasks.get(i).getTaskId(), subTasks.get(i).getName(),
                            subTasks.get(i).getDescription() });
        }

        var tableData = new ArrayList<String[]>();
        tableData.add(header);
        tableData.addAll(subtasksStringArray);
        ArrayTableModel model = new ArrayTableModel(tableData.toArray(String[][]::new));
        TableBuilder tableBuilder = new TableBuilder(model);
        tableBuilder.addInnerBorder(BorderStyle.fancy_light_double_dash);
        return tableBuilder;
    }

}
