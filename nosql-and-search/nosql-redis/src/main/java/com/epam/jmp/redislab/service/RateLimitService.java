package com.epam.jmp.redislab.service;

import java.util.Set;

import com.epam.jmp.redislab.api.RequestDescriptor;

public interface RateLimitService {

    default boolean shouldLimit(Set<RequestDescriptor> requestDescriptors) {
        return false;
    }

}
