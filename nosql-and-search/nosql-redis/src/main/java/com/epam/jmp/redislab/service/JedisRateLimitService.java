package com.epam.jmp.redislab.service;

import java.util.*;

import org.springframework.stereotype.Component;

import com.epam.jmp.redislab.api.RequestDescriptor;
import com.epam.jmp.redislab.configuration.ratelimit.RateLimitRule;
import com.epam.jmp.redislab.configuration.ratelimit.RateLimitTimeInterval;

import redis.clients.jedis.JedisCluster;

@Component
public class JedisRateLimitService implements RateLimitService {

    private final List<RateLimitRule> rateLimitRules;
    private final JedisCluster jedisCluster;

    public JedisRateLimitService(Set<RateLimitRule> rateLimitRules, JedisCluster jedisCluster) {
        this.rateLimitRules = new ArrayList<>(rateLimitRules.stream().toList());
        this.jedisCluster = jedisCluster;
    }

    @Override
    public boolean shouldLimit(Set<RequestDescriptor> requestDescriptors) {
        for (RequestDescriptor request : requestDescriptors) {
            var accountId = request.getAccountId().orElse("");
            var clientId = request.getClientIp().orElse("");
            var requestType = request.getRequestType().orElse("");

            Collections.sort(this.rateLimitRules);

            var rule = this.rateLimitRules.stream().filter(rateRule -> {
                var ruleAccountId = rateRule.getAccountId().orElse("");
                var ruleClientId = rateRule.getClientIp().orElse("");
                var ruleRequestType = rateRule.getRequestType().orElse("");
                return (accountId.equals(ruleAccountId) || ruleAccountId.equals(""))
                        && (Objects.equals(clientId, ruleClientId) || ruleClientId.equals(""))
                        && (Objects.equals(requestType, ruleRequestType) ||
                                ruleRequestType.equals(""));
            })
                    .findFirst().get();

            var allowedNumberOfRequests = rule.allowedNumberOfRequests();
            var interval = rule.rateLimitTimeInterval();
            int expireSeconds = interval.equals(RateLimitTimeInterval.MINUTE) ? 60 : 3600;
            String key = String.format("%s-%s-%s", accountId, clientId, requestType);

            var existentValue = this.jedisCluster.get(key);
            if (existentValue == null) {
                this.jedisCluster.setex(key, expireSeconds, String.valueOf(--allowedNumberOfRequests));
            } else {
                if (Integer.valueOf(existentValue) < 1) {
                    return true;
                } else {
                    long currentExpiration = this.jedisCluster.ttl(key);
                    int data = Integer.valueOf(existentValue) - 1;
                    this.jedisCluster.set(key, String.valueOf(data));
                    this.jedisCluster.expire(key, currentExpiration);
                }
            }
        }
        return false;
    }
}
