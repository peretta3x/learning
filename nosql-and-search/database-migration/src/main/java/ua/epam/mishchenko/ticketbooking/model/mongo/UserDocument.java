package ua.epam.mishchenko.ticketbooking.model.mongo;

import java.math.BigDecimal;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("users")
public class UserDocument {

    @Id
    private String id;

    @Indexed
    private String name;

    private String email;

    private BigDecimal money;

    public UserDocument(String id, String name, String email, BigDecimal money) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.money = money;
    }

    public UserDocument() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "UserDocument [id=" + id + ", name=" + name + ", email=" + email + ", money=" + money + "]";
    }

}
