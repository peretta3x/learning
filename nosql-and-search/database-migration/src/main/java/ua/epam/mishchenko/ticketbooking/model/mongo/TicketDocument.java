package ua.epam.mishchenko.ticketbooking.model.mongo;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import ua.epam.mishchenko.ticketbooking.model.Category;

@Document("tickets")
public class TicketDocument {
    @Id
    private String id;

    @Field(name = "user_id", targetType = FieldType.OBJECT_ID)
    private String userId;

    @Field(name = "event_id", targetType = FieldType.OBJECT_ID)
    private String eventId;

    private Integer place;

    private Category category;

    public TicketDocument(String id, String userId, String eventId, Integer place, Category category) {
        this.id = id;
        this.userId = userId;
        this.eventId = eventId;
        this.place = place;
        this.category = category;
    }

    public TicketDocument() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "TicketDocument [id=" + id + ", userId=" + userId + ", eventId=" + eventId + ", place=" + place
                + ", category=" + category + "]";
    }
}
