package ua.epam.mishchenko.ticketbooking.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.epam.mishchenko.ticketbooking.model.Event;
import ua.epam.mishchenko.ticketbooking.model.Ticket;
import ua.epam.mishchenko.ticketbooking.model.User;
import ua.epam.mishchenko.ticketbooking.model.mongo.EventDocument;
import ua.epam.mishchenko.ticketbooking.model.mongo.TicketDocument;
import ua.epam.mishchenko.ticketbooking.model.mongo.UserDocument;
import ua.epam.mishchenko.ticketbooking.model.mongo.UserWithTitckets;
import ua.epam.mishchenko.ticketbooking.repository.EventRepository;
import ua.epam.mishchenko.ticketbooking.repository.TicketRepository;
import ua.epam.mishchenko.ticketbooking.repository.UserRepository;

@Service
public class MigrationToMongoDB implements CommandLineRunner {

    private final MongoTemplate mongoTemplate;

    private final EventRepository eventRepository;
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;

    public MigrationToMongoDB(MongoTemplate mongoTemplate, EventRepository eventRepository,
            UserRepository userRepository, TicketRepository ticketRepository) {
        this.mongoTemplate = mongoTemplate;
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
        this.ticketRepository = ticketRepository;
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        migratingEvents();
        migratingUsers();
        migratingTickets();

        System.out.println("########## IMPORTING FROM SQL TO MONGO HAS BEEN COMPLETED ##########");
        List<EventDocument> eventDocuments = this.mongoTemplate.findAll(EventDocument.class);
        List<UserDocument> userDocuments = this.mongoTemplate.findAll(UserDocument.class);
        List<TicketDocument> ticketDocuments = this.mongoTemplate.findAll(TicketDocument.class);

        System.out.println("### Events ###");
        eventDocuments.forEach(System.out::println);
        System.out.println("### Users ###");
        userDocuments.forEach(System.out::println);
        System.out.println("### Tickets ###");
        ticketDocuments.forEach(System.out::println);

        System.out.println("\n\n\n##########EXAMPLE USING AGGREGATION #########");
        System.out.println("\n########## SHOWING USERS AND ITS TICKETS ########");
        this.getUsersWithTickets().forEach(System.out::println);
    }

    private void migratingTickets() {
        Iterable<Ticket> tickets = this.ticketRepository.findAll();
        tickets.forEach(ticket -> {
            String userId = Util.getUserId(ticket.getUser().getId());
            String eventId = Util.getEventId(ticket.getEvent().getId());
            TicketDocument ticketDocument = new TicketDocument(null, userId,
                    eventId,
                    ticket.getPlace(),
                    ticket.getCategory());

            this.mongoTemplate.insert(ticketDocument);
        });
    }

    private void migratingUsers() {
        Iterable<User> users = this.userRepository.findAll();
        users.forEach(user -> {
            UserDocument userDocument = new UserDocument(null, user.getName(), user.getEmail(),
                    user.getUserAccount().getMoney());

            String userId = this.mongoTemplate.insert(userDocument).getId();
            Util.addUserId(user.getId(), userId);
        });
    }

    private void migratingEvents() {
        Iterable<Event> events = this.eventRepository.findAll();
        events.forEach(event -> {
            EventDocument eventDocument = new EventDocument(null, event.getTitle(), event.getDate(),
                    event.getTicketPrice());
            String eventId = this.mongoTemplate.insert(eventDocument).getId();
            Util.addEventId(event.getId(), eventId);
        });
    }

    public List<UserWithTitckets> getUsersWithTickets() {
        LookupOperation lookupOperation = LookupOperation.newLookup()
                .from("tickets")
                .localField("_id")
                .foreignField("user_id")
                .as("tickets");
        TypedAggregation<UserDocument> aggregation = Aggregation.newAggregation(UserDocument.class, lookupOperation);
        AggregationResults<UserWithTitckets> results = mongoTemplate.aggregate(aggregation, UserWithTitckets.class);
        return results.getMappedResults();
    }

}

class Util {
    private static Map<Long, String> eventsId = new HashMap<>();
    private static Map<Long, String> usersId = new HashMap<>();

    static void addEventId(Long pgId, String mongoId) {
        eventsId.put(pgId, mongoId);
    }

    static void addUserId(Long pgId, String mongoId) {
        usersId.put(pgId, mongoId);
    }

    static String getEventId(Long id) {
        return eventsId.get(id);
    }

    static String getUserId(Long id) {
        return usersId.get(id);
    }
}
