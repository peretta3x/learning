package ua.epam.mishchenko.ticketbooking.model.mongo;

import java.util.List;

public class UserWithTitckets {
    private String id;
    private String name;
    private String email;

    List<TicketDocument> tickets;

    public UserWithTitckets(String id, String name, String email, List<TicketDocument> tickets) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.tickets = tickets;
    }

    public UserWithTitckets() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TicketDocument> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDocument> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "UserWithTitckets [id=" + id + ", name=" + name + ", email=" + email + ", tickets=" + tickets + "]";
    }
}
