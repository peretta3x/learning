package com.epam.rafaelperetta.client;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface EmployeeGateway {

    List<Employee> findAll();

    Optional<Employee> findById(String id);

    boolean create(Employee employee);

    boolean update(String id, UpdateRequest<Employee> updateRequest);

    boolean deleteById(String id);

    List<Employee> search(String fieldName, String value);

    Collection<AggregationResponse> aggregate(String metric, String field);

}