package com.epam.rafaelperetta.client;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    private final EmployeeGateway employeeService;

    private static final List<String> NUMERIC_FIELDS = List.of("experience", "rating", "salary");
    private static final List<String> NUMERIC_METRIC_SINGLE_VALUE = List.of("min", "max", "sum", "avg");

    public EmployeeController(EmployeeGateway employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Employee> getAll() {
        return this.employeeService.findAll();
    }

    @GetMapping("/search")
    public List<Employee> getAll(@RequestParam String field, String value) {
        return this.employeeService.search(field, value);
    }

    @GetMapping("/aggregation")
    public ResponseEntity<Collection<AggregationResponse>> getAggregation(@RequestParam String metric, String field) {
        if (!NUMERIC_METRIC_SINGLE_VALUE.contains(metric) || !NUMERIC_FIELDS.contains(field)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(this.employeeService.aggregate(metric, field));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getById(@PathVariable("id") String id) {
        Optional<Employee> employeeOptional = this.employeeService.findById(id);
        if (employeeOptional.isPresent()) {
            return ResponseEntity.ok().body(employeeOptional.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Void> createEmployee(@RequestBody Employee employee) {
        if (this.employeeService.create(employee)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable String id) {
        if (this.employeeService.deleteById(id)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> createEmployee(@PathVariable String id, @RequestBody Employee employee) {
        UpdateRequest<Employee> updateRequest = new UpdateRequest<>(employee);
        if (this.employeeService.update(id, updateRequest)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

}
