package com.epam.rafaelperetta.client.highlevel;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.epam.rafaelperetta.client.AggregationResponse;
import com.epam.rafaelperetta.client.Employee;
import com.epam.rafaelperetta.client.EmployeeGateway;
import com.epam.rafaelperetta.client.UpdateRequest;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation.Builder;
import co.elastic.clients.elasticsearch._types.aggregations.SingleMetricAggregateBase;
import co.elastic.clients.elasticsearch.core.GetResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.util.ObjectBuilder;

@ConditionalOnProperty(name = "elasticsearch.api.level", havingValue = "high")
@Service
public class HighLevelEmployeeService implements EmployeeGateway {

    public static Logger logger = LoggerFactory.getLogger(HighLevelEmployeeService.class);

    private final ElasticsearchClient elasticsearchClient;

    public HighLevelEmployeeService(ElasticsearchClient elasticsearchClient) {
        this.elasticsearchClient = elasticsearchClient;
    }

    @Override
    public List<Employee> findAll() {
        try {
            SearchResponse<Employee> response = elasticsearchClient.search(s -> s
                    .index("employees")
                    .size(1000)
                    .from(0),
                    Employee.class);

            List<Employee> employees = response.hits().hits()
                    .stream()
                    .map(hit -> {
                        String id = hit.id();
                        Employee employee = hit.source();
                        employee.setId(id);
                        return employee;
                    }).toList();
            logger.info("Total of employees is {}", employees.size());
            return employees;
        } catch (ElasticsearchException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return List.of();
    }

    @Override
    public Optional<Employee> findById(String id) {
        GetResponse<Employee> response;
        try {
            response = elasticsearchClient.get(g -> g
                    .index("employees")
                    .id(id),
                    Employee.class);

            if (response.found()) {
                Employee employee = response.source();
                employee.setId(response.id());
                logger.info("Employee name " + employee.getName());
                return Optional.of(employee);
            } else {
                logger.info("Employee not found");
                return Optional.empty();
            }
        } catch (ElasticsearchException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public boolean create(Employee employee) {
        try {
            elasticsearchClient.index(u -> u
                    .index("employees").document(employee));
            return true;
        } catch (ElasticsearchException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(String id, UpdateRequest<Employee> updateRequest) {
        try {
            elasticsearchClient.update(u -> u
                    .index("employees")
                    .id(id)
                    .doc(updateRequest.getDoc()),
                    Employee.class);
            return true;
        } catch (ElasticsearchException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteById(String id) {
        try {
            elasticsearchClient.delete(d -> d.index("employees").id(id));
            return true;
        } catch (ElasticsearchException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Employee> search(String fieldName, String value) {
        try {
            SearchResponse<Employee> response = elasticsearchClient.search(s -> s
                    .index("employees")
                    .size(1000)
                    .from(0)
                    .query(q -> q.match(t -> t.field(fieldName).query(value))),
                    Employee.class);

            List<Employee> employees = response.hits().hits()
                    .stream()
                    .map(hit -> {
                        String id = hit.id();
                        Employee employee = hit.source();
                        employee.setId(id);
                        return employee;
                    }).toList();
            logger.info("Total of employees is {}", employees.size());
            return employees;
        } catch (ElasticsearchException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return List.of();
    }

    @Override
    public Collection<AggregationResponse> aggregate(String metric, String field) {
        Function<Builder, ObjectBuilder<Aggregation>> minAggregation = a -> a.min(m -> m.field(field));
        Function<Builder, ObjectBuilder<Aggregation>> maxAggregation = a -> a.max(m -> m.field(field));
        Function<Builder, ObjectBuilder<Aggregation>> sumAggregation = a -> a.sum(s -> s.field(field));
        Function<Builder, ObjectBuilder<Aggregation>> valueCountAggregation = a -> a.valueCount(v -> v.field(field));
        Function<Builder, ObjectBuilder<Aggregation>> avgAggregation = a -> a.avg(avg -> avg.field(field));

        var aggregations = Map.of("min", minAggregation, "max", maxAggregation, "sum", sumAggregation, "value_count",
                valueCountAggregation, "avg", avgAggregation);

        var selectedMetricAggregation = aggregations.getOrDefault(metric, valueCountAggregation);

        try {
            SearchResponse<Employee> response = elasticsearchClient.search(s -> s
                    .index("employees")
                    .size(0)
                    .aggregations("%s_%s".formatted(metric, field), selectedMetricAggregation),
                    Employee.class);

            return response.aggregations().values().stream()
                    .map(a -> ((SingleMetricAggregateBase) a._get()))
                    .map(a -> AggregationResponse.of(a.value()))
                    .toList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

}
