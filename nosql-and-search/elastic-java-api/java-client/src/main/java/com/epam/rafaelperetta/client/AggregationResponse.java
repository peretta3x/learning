package com.epam.rafaelperetta.client;

public record AggregationResponse(double value) {

    public static AggregationResponse of(double value) {
        return new AggregationResponse(value);
    }

}
