package com.epam.rafaelperetta.client.config;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SpringDocConfig {

    @Bean
    GroupedOpenApi controllerApi() {
        return GroupedOpenApi.builder()
                .group("employee-api")
                .packagesToScan("com.epam.rafaelperetta.client") // Specify the package to scan
                .build();
    }

}