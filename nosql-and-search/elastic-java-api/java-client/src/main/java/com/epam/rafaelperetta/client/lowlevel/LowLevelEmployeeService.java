package com.epam.rafaelperetta.client.lowlevel;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.epam.rafaelperetta.client.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ConditionalOnProperty(name = "elasticsearch.api.level", havingValue = "low")
@Service
public class LowLevelEmployeeService implements EmployeeGateway {

    public static Logger logger = LoggerFactory.getLogger(LowLevelEmployeeService.class);

    private final RestClient restClient;

    private final ObjectMapper objectMapper;

    public LowLevelEmployeeService(RestClient restClient, ObjectMapper objectMapper) {
        this.restClient = restClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> employees = new ArrayList<>();
        try {
            Request request = new Request("GET", "/employees/_search?size=1000&from=0");
            Response response = this.restClient.performRequest(request);
            String responseBody = EntityUtils.toString(response.getEntity());

            JsonNode jsonNode = objectMapper.readValue(responseBody, JsonNode.class);
            JsonNode hits = jsonNode.get("hits").get("hits");

            for (JsonNode hit : hits) {
                employees.add(jsonNodeToEmployee(hit));
            }
        } catch (IOException e) {
            logger.error("Error to get all employees", e);
        }

        logger.info("Total of employees is {}", employees.size());
        return employees;
    }

    @Override
    public Optional<Employee> findById(String id) {
        try {
            Request request = new Request("GET", "/employees/_doc/" + id);
            Response response = this.restClient.performRequest(request);
            if (response.getStatusLine().getStatusCode() == 404) {
                return Optional.empty();
            }
            String responseBody = EntityUtils.toString(response.getEntity());

            JsonNode jsonNode = objectMapper.readValue(responseBody, JsonNode.class);
            Employee employee = jsonNodeToEmployee(jsonNode);
            return Optional.of(employee);
        } catch (Exception e) {
            logger.error("Error to get employee by id: {}", id, e);
        }
        return Optional.empty();
    }

    @Override
    public boolean create(Employee employee) {
        try {
            Request request = new Request("POST", "/employees/_doc");
            String employeeJson = this.objectMapper.writeValueAsString(employee);
            request.setJsonEntity(employeeJson);
            Response response = this.restClient.performRequest(request);
            if (response.getStatusLine().getStatusCode() == 201) {
                logger.info("Document created successfully");
                System.out.println("Document created successfully");
                return true;
            } else {
                logger.info("Failed to create document");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(String id, UpdateRequest<Employee> updateRequest) {
        try {
            Request request = new Request("POST", "/employees/_update/" + id);
            String updateJson = this.objectMapper.writeValueAsString(updateRequest);
            request.setJsonEntity(updateJson);
            Response response = this.restClient.performRequest(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("Document updated successfully");
                return true;
            } else {
                logger.info("Failed to update document");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteById(String id) {
        try {
            Request request = new Request("DELETE", "/employees/_doc/" + id);
            Response response = this.restClient.performRequest(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("Document deleted successfully");
                return true;
            } else if (response.getStatusLine().getStatusCode() == 404) {
                logger.info("Document not found");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private Employee jsonNodeToEmployee(JsonNode employeeNode) {
        String id = employeeNode.get("_id").asText();

        JsonNode employeeSource = employeeNode.get("_source");

        String name = employeeSource.get("name").asText();
        String dob = employeeSource.get("dob").asText();
        // address
        String country = employeeSource.get("address").get("country").asText();
        String town = employeeSource.get("address").get("town").asText();
        Address address = new Address(country, town);

        String email = employeeSource.get("email").asText();

        // skills
        List<String> skills = new ArrayList<>();
        for (JsonNode skill : employeeSource.get("skills")) {
            skills.add(skill.textValue());
        }

        int experience = employeeSource.get("experience").asInt();
        double rating = employeeSource.get("rating").asDouble();
        boolean verified = employeeSource.get("verified").asBoolean();
        BigDecimal salary = new BigDecimal(employeeSource.get("salary").asText());

        Employee employee = new Employee(id, name, dob, address, email, skills, experience, rating, email, verified,
                salary);

        return employee;
    }

    @Override
    public List<Employee> search(String fieldName, String value) {
        String jsonString = """
                {
                "query": {
                    "match": {
                    "%s": "%s"
                    }
                }
                }
                """.formatted(fieldName, value);
        List<Employee> employees = new ArrayList<>();
        try {
            Request request = new Request("GET", "/employees/_search?size=1000&from=0");
            request.setJsonEntity(jsonString);
            Response response = this.restClient.performRequest(request);
            String responseBody = EntityUtils.toString(response.getEntity());

            JsonNode jsonNode = objectMapper.readValue(responseBody, JsonNode.class);
            JsonNode hits = jsonNode.get("hits").get("hits");

            for (JsonNode hit : hits) {
                employees.add(jsonNodeToEmployee(hit));
            }
        } catch (IOException e) {
            logger.error("Error to get employees with %s=%s".formatted(fieldName, value), e);
        }

        logger.info("Total of employees is {}", employees.size());
        return employees;
    }

    @Override
    public Collection<AggregationResponse> aggregate(String metric, String field) {
        var aggregationName = "%s_%s".formatted(metric, field);
        var jsonString = """
                {
                  "aggs": {
                    "%s": {
                      "%s": {
                        "field": "%s"
                      }
                    }
                  }
                }
                """.formatted(aggregationName, metric, field);
        try {
            Request request = new Request("GET", "/employees/_search?size=0");
            request.setJsonEntity(jsonString);
            Response response = this.restClient.performRequest(request);
            String responseBody = EntityUtils.toString(response.getEntity());
            JsonNode jsonNode = objectMapper.readValue(responseBody, JsonNode.class);
            var aggregationValue = jsonNode
                    .get("aggregations")
                    .get(aggregationName)
                    .get("value").asDouble();

            return List.of(AggregationResponse.of(aggregationValue));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

}
