package com.epam.rafaelperetta.client;

public class UpdateRequest<T> {

    private T doc;

    public UpdateRequest(T doc) {
        this.doc = doc;
    }

    public T getDoc() {
        return doc;
    }

    public void setDoc(T doc) {
        this.doc = doc;
    }

}
